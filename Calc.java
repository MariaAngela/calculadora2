import java.util.*;

public class Calc { //Contexto
	Map<String, Operation> operations;

	public Calc() {
		operations = new HashMap<String, Operation>();
		register("+", new Sum());
		register("-", new Substraction());
		register("@", new Arroba());
	}

	public void register(String operator, Operation operation){
		operations.put(operator, operation);
	}

	public int operate(String operator, int a, int b){
		Operation operation = operations.get(operator);
		return operation.operate(a,b);
	}
}

abstract class Operation { //Estrategia
	public abstract int operate(int a, int b);
}

class Sum extends Operation {
	public int operate(int a, int b ){
	return a + b;
	}
}


class Substraction extends Operation {
	public int operate(int a, int b ){
	return a - b;
	}
}

class Arroba extends Operation {
	public int operate(int a, int b ){
	return (b*(a-2)/(a+b));
	}
}

